package com.nextgen.spsl;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.nextgen.spsl.Network.APIClient;
import com.nextgen.spsl.Network.APIRequests;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    Button register,login;
    EditText user_id,password;
    String str_user_id="",str_password="";
    Context context;
    ProgressDialog progressDialog;
    private  static APIRequests apiRequests;

    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        context=this;
        apiRequests = APIClient.getInstance().create(APIRequests.class);
        progressDialog=new ProgressDialog(context);
        progressDialog.setMessage("Please Wait");
        register=findViewById(R.id.register);
        login=findViewById(R.id.login);
        user_id=findViewById(R.id.user_id);
        password=findViewById(R.id.password);

        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               checkValidation();
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(LoginActivity.this,RegistrationActivity.class);
                startActivity(i);
            }
        });
    }

    public void checkValidation()
    {
        str_user_id=user_id.getText().toString();
        str_password=password.getText().toString();

        if(str_user_id.equalsIgnoreCase(""))
        {
            Toast.makeText(this,"Please Enter The UserId",Toast.LENGTH_SHORT).show();
            return;
        }
        if(str_password.equalsIgnoreCase(""))
        {
            Toast.makeText(this,"Please Enter The Password",Toast.LENGTH_SHORT).show();
            return;
        }


        progressDialog.show();
        Call<JsonElement> aboutCall= apiRequests.login(str_user_id,str_password);
        aboutCall.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response)
            {
                try
                {
                    JSONObject jsonObject=new JSONObject(response.body().toString());

                    if(jsonObject.getString("res_status").equalsIgnoreCase("200"))
                    {
                        progressDialog.cancel();

                        JSONArray jsonArray=jsonObject.getJSONArray("res_data");

                        JSONObject jsonObject1=jsonArray.getJSONObject(0);

                        String id=jsonObject1.getString("id");
                        String name=jsonObject1.getString("name");
                        String email=jsonObject1.getString("email");
                        String password=jsonObject1.getString("password");
                        String address=jsonObject1.getString("address");
                        String mobile=jsonObject1.getString("mobile");
                        String remark=jsonObject1.getString("remark");
                        String usertype=jsonObject1.getString("usertype");

                        editor.putBoolean("islogin",true);
                        editor.putString("id",id);
                        editor.putString("name",name);
                        editor.putString("email",email);
                        editor.putString("password",password);
                        editor.putString("address",address);
                        editor.putString("mobile",mobile);
                        editor.putString("remark",remark);
                        editor.putString("usertype",usertype);
                        editor.commit();


                        Toast.makeText(context,"Success!",Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(LoginActivity.this,DashboardActivity.class);
                        startActivity(i);
                        finish();

                    }
                    else
                    {
                        progressDialog.cancel();
                        Toast.makeText(context,"Oops Went Something wrong...",Toast.LENGTH_SHORT).show();

                    }


                }
                catch (Exception e)
                {
                    progressDialog.cancel();
                    Toast.makeText(context,"Oops Went Something wrong...",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.cancel();
                Toast.makeText(context,"Oops Went Something wrong...",Toast.LENGTH_SHORT).show();
            }
        });
    }



    }



