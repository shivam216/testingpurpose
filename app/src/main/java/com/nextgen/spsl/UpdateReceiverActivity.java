package com.nextgen.spsl;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.nextgen.spsl.Network.APIClient;
import com.nextgen.spsl.Network.APIRequests;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateReceiverActivity extends AppCompatActivity {

    EditText name,query,mobile,address,acno,ifsc,branch;
    String str_name="",str_query="",str_mobile="",str_address="",str_acno="",str_ifsc="",str_branch="";
    Button submit;

    private  static APIRequests apiRequests;
    Context context;
    ProgressDialog progressDialog;

    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_receiver);
        context=this;
        apiRequests = APIClient.getInstance().create(APIRequests.class);
        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        progressDialog=new ProgressDialog(context);
        progressDialog.setMessage("Please Wait");

        name=findViewById(R.id.name);
        query=findViewById(R.id.query);
        mobile=findViewById(R.id.mobile);
        address=findViewById(R.id.address);
        acno=findViewById(R.id.acno);
        ifsc=findViewById(R.id.ifsc);
        branch=findViewById(R.id.branch);
        submit=findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValidation();
            }
        });
    }

    public void checkValidation()
    {
        str_name=name.getText().toString();
        str_query=query.getText().toString();
        str_mobile=mobile.getText().toString();
        str_address=address.getText().toString();

        str_acno=acno.getText().toString();
        str_ifsc=ifsc.getText().toString();
        str_branch=branch.getText().toString();

        if(str_name.equalsIgnoreCase(""))
        {
            Toast.makeText(this,"Please Enter The Name",Toast.LENGTH_SHORT).show();
            return;
        }
        if(str_query.equalsIgnoreCase(""))
        {
            Toast.makeText(this,"Please Enter Your Query",Toast.LENGTH_SHORT).show();
            return;
        }
        if(str_mobile.equalsIgnoreCase(""))
        {
            Toast.makeText(this,"Please Enter Your Mobile No",Toast.LENGTH_SHORT).show();
            return;
        }

        if(str_address.equalsIgnoreCase(""))
        {
            Toast.makeText(this,"Please Enter Your Address",Toast.LENGTH_SHORT).show();
            return;
        }
        progressDialog.show();
        Call<JsonElement> aboutCall= apiRequests.receive(sharedPreferences.getString("id","0"),str_name,str_query,str_mobile,str_address,str_acno,str_ifsc,str_branch);
        aboutCall.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response)
            {
                try
                {
                    JSONObject jsonObject=new JSONObject(response.body().toString());

                    if(jsonObject.getString("res_status").equalsIgnoreCase("200"))
                    {
                        progressDialog.cancel();
                        Toast.makeText(context,"Success!",Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(UpdateReceiverActivity.this,DashboardActivity.class);
                        startActivity(i);
                    }
                    else
                    {
                        progressDialog.cancel();
                        Toast.makeText(context,"Oops Went Something wrong...",Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    progressDialog.cancel();
                    Toast.makeText(context,"Oops Went Something wrong...",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.cancel();
                Toast.makeText(context,"Oops Went Something wrong...",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
