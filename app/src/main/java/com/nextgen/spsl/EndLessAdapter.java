package com.nextgen.spsl;

import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

public class EndLessAdapter extends PagerAdapter {
    public static int pos = 0;
    FragmentActivity activity;
    int imageArray[];

    public EndLessAdapter(FragmentActivity act, int[] imgArra) {
        imageArray = imgArra;
        activity = act;
    }

    public int getCount() {
        return Integer.MAX_VALUE;
    }



    public Object instantiateItem(View collection, int position) {

        ImageView mwebView = new ImageView(activity);
        ((ViewPager) collection).addView(mwebView, 0);
        mwebView.setScaleType(ScaleType.FIT_XY);
        mwebView.setImageResource(imageArray[pos]);

        if (pos >= imageArray.length - 1)
            pos = 0;
        else
            ++pos;

        return mwebView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);

    }



    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == ((View) arg1);
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}
