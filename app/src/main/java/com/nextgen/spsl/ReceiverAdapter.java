package com.nextgen.spsl;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ReceiverAdapter extends RecyclerView.Adapter<ReceiverAdapter.ViewHolder>{
    ArrayList<ReceiverModal> notificationModalArrayList=new ArrayList<>();
    Context c;

    public ReceiverAdapter(Context c, ArrayList<ReceiverModal> listdata) {
        this.notificationModalArrayList = listdata;
        this.c = c;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_receiver_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.name.setText("Name :"+notificationModalArrayList.get(position).getName());
        holder.query.setText("Query :"+notificationModalArrayList.get(position).getQuery());
        holder.mobile.setText("Mobile :"+notificationModalArrayList.get(position).getMbile());
        holder.address.setText("Address :"+notificationModalArrayList.get(position).getAddress());
        holder.rdate.setText("Date :"+notificationModalArrayList.get(position).getDate_time());

        String s=notificationModalArrayList.get(position).getAcno();

        try
        {
            holder.acno.setText("Ac No :XXXXXXXXXX"+s.substring(s.length() - 3));

        }
        catch (Exception e)
        {
            e.printStackTrace();

            holder.acno.setText("Ac No :...");

        }


       // System.out.println(s.substring(0, s.length() - 2));




        if(notificationModalArrayList.get(position).getStatus().equalsIgnoreCase("0"))
        {
            holder.status.setText("Status :Pending");
            holder.status.setTextColor(c.getColor(R.color.sitelightred));

        }
        else
        {
            holder.status.setText("Status :Completed");
            holder.status.setTextColor(c.getColor(R.color.colorAccent));
        }

    }

    @Override
    public int getItemCount() {
        return notificationModalArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView query;
        public TextView mobile;
        public TextView address;
        public TextView rdate;
        public TextView acno;
        public TextView status;


        public ViewHolder(View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.name);
            this.query = itemView.findViewById(R.id.query);
            this.mobile = itemView.findViewById(R.id.mobile);
            this.address = itemView.findViewById(R.id.address);
            this.rdate = itemView.findViewById(R.id.rdate);
            this.acno = itemView.findViewById(R.id.acno);
            this.status = itemView.findViewById(R.id.status);


        }
    }
}