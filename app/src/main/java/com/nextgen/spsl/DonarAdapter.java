package com.nextgen.spsl;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class DonarAdapter extends RecyclerView.Adapter<DonarAdapter.ViewHolder>{
    ArrayList<DonarModal> notificationModalArrayList=new ArrayList<>();
    Context c;

    public DonarAdapter(Context c, ArrayList<DonarModal> listdata) {
        this.notificationModalArrayList = listdata;
        this.c = c;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_recharge_report, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.amount.setText("Amount :Rs"+notificationModalArrayList.get(position).getAmount());
        holder.trans_id.setText("Transaction Id :"+notificationModalArrayList.get(position).getTransaction_id());
        holder.remark.setText("Remark :"+notificationModalArrayList.get(position).getRemark());
        holder.rdate.setText("Date :"+notificationModalArrayList.get(position).getDate_time());
    }

    @Override
    public int getItemCount() {
        return notificationModalArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView amount;
        public TextView trans_id;
        public TextView remark;
        public TextView rdate;


        public ViewHolder(View itemView) {
            super(itemView);
            this.amount = itemView.findViewById(R.id.amount);
            this.trans_id = itemView.findViewById(R.id.trans_id);
            this.remark = itemView.findViewById(R.id.remark);
            this.rdate = itemView.findViewById(R.id.rdate);


        }
    }


}