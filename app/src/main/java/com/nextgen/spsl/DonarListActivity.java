package com.nextgen.spsl;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.nextgen.spsl.Network.APIClient;
import com.nextgen.spsl.Network.APIRequests;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DonarListActivity extends AppCompatActivity {


    Context context;
    ProgressDialog progressDialog;
    private  static APIRequests apiRequests;

    ArrayList<DonarModal> donarModalArrayList;

    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_donar_list);
        recyclerView=findViewById(R.id.recycler_view);
        context=this;
        apiRequests = APIClient.getInstance().create(APIRequests.class);
       progressDialog=new ProgressDialog(context);
       progressDialog.setMessage("Please Wait..");
        getReport();
    }



    public void getReport()
    {
       progressDialog.show();
        Call<JsonElement> aboutCall= apiRequests.getData("my-master.php?api_key=NEXTGENSOL@123&action=getDonar");
        aboutCall.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response)
            {
                try
                {
                    parseData(response.body().toString());
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    Toast.makeText(context,"Oops Went Something wrong...",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    public void parseData(String response)
    {
        donarModalArrayList=new ArrayList<>();
        try {
            JSONObject jsonObject=new JSONObject(response);

            String status=jsonObject.getString("res_status");

            if(status.equalsIgnoreCase("200") )
            {
                JSONArray jsonArray=jsonObject.getJSONArray("res_data");
                for (int i=0;i<jsonArray.length();i++) {
                    DonarModal rechargeReportModal = new DonarModal();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    String id = jsonObject1.getString("id");
                    rechargeReportModal.setId(id);


                    String userid = jsonObject1.getString("userid");
                    rechargeReportModal.setUserid(userid);

                    String amount = jsonObject1.getString("amount");
                    rechargeReportModal.setAmount(amount);

                    String transaction_id = jsonObject1.getString("transaction_id");
                    rechargeReportModal.setTransaction_id(transaction_id);

                    String remark = jsonObject1.getString("remark");
                    rechargeReportModal.setRemark(remark);

                    String date_time = jsonObject1.getString("date_time");
                    rechargeReportModal.setDate_time(date_time);

                    donarModalArrayList.add(rechargeReportModal);

                }
                progressDialog.dismiss();
                DonarAdapter jobAdapter = new DonarAdapter(context,donarModalArrayList);
                recyclerView.setAdapter(jobAdapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            }
            else
            {
                progressDialog.dismiss();
            }
        }
        catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
        }
    }

}
