package com.nextgen.spsl;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.nextgen.spsl.Network.APIClient;
import com.nextgen.spsl.Network.APIRequests;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceiverListActivity extends AppCompatActivity {


    Context context;
    ProgressDialog progressDialog;
    private  static APIRequests apiRequests;

    ArrayList<ReceiverModal> receiverModalArrayList;

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_receiver_list);

        recyclerView=findViewById(R.id.recycler_view);
        context=this;
        apiRequests = APIClient.getInstance().create(APIRequests.class);
        progressDialog=new ProgressDialog(context);
        progressDialog.setMessage("Please Wait..");
        getReport();

    }


    public void getReport()
    {
        progressDialog.show();
        Call<JsonElement> aboutCall= apiRequests.getData("my-master.php?api_key=NEXTGENSOL@123&action=getReceiver");
        aboutCall.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response)
            {
                try
                {
                    parseData(response.body().toString());
                }
                catch (Exception e)
                {
                    progressDialog.dismiss();
                    Toast.makeText(context,"Oops Went Something wrong...",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    public void parseData(String response)
    {
        receiverModalArrayList=new ArrayList<>();
        try {
            JSONObject jsonObject=new JSONObject(response);

            String status=jsonObject.getString("res_status");

            if(status.equalsIgnoreCase("200") )
            {
                JSONArray jsonArray=jsonObject.getJSONArray("res_data");
                for (int i=0;i<jsonArray.length();i++) {
                    ReceiverModal rechargeReportModal = new ReceiverModal();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    String id = jsonObject1.getString("id");
                    rechargeReportModal.setId(id);


                    String userid = jsonObject1.getString("user_id");
                    rechargeReportModal.setUser_id(userid);

                    String name = jsonObject1.getString("name");
                    rechargeReportModal.setName(name);

                    String query = jsonObject1.getString("query");
                    rechargeReportModal.setQuery(query);

                    String mobile = jsonObject1.getString("mobile");
                    rechargeReportModal.setMbile(mobile);

                    String address = jsonObject1.getString("address");
                    rechargeReportModal.setAddress(address);

                    String date_time = jsonObject1.getString("date_time");
                    rechargeReportModal.setDate_time(date_time);


                    String acno = jsonObject1.getString("acno");
                    rechargeReportModal.setAcno(acno);

                    String ifsc = jsonObject1.getString("ifsc");
                    rechargeReportModal.setIfsc(ifsc);

                    String branch = jsonObject1.getString("branch");
                    rechargeReportModal.setBranch(branch);

                    String statuss = jsonObject1.getString("status");
                    rechargeReportModal.setStatus(statuss);

                    receiverModalArrayList.add(rechargeReportModal);

                }
                progressDialog.dismiss();

                try {
                    ReceiverAdapter jobAdapter = new ReceiverAdapter(context,receiverModalArrayList);
                    recyclerView.setAdapter(jobAdapter);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
            else
            {
                progressDialog.dismiss();
            }
        }
        catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
        }
    }

}
