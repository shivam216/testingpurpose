package com.nextgen.spsl;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.nextgen.spsl.Network.APIClient;
import com.nextgen.spsl.Network.APIRequests;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateDonarActivity extends AppCompatActivity {

    EditText amount,trans_id,remark;
    String str_amount="",str_trans_id="",str_remark="";
    Button submit;

    private  static APIRequests apiRequests;
    Context context;
    ProgressDialog progressDialog;

    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_donar);
        context=this;
        apiRequests = APIClient.getInstance().create(APIRequests.class);
        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        progressDialog=new ProgressDialog(context);
        progressDialog.setMessage("Please Wait");

        amount=findViewById(R.id.amount);
        trans_id=findViewById(R.id.trans_id);
        remark=findViewById(R.id.remark);
        submit=findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkValidation();

            }
        });
    }

    public void checkValidation()
    {
        str_amount=amount.getText().toString();
        str_remark=remark.getText().toString();
        str_trans_id=trans_id.getText().toString();

        if(str_amount.equalsIgnoreCase(""))
        {
            Toast.makeText(this,"Please Enter The Amount in Rs",Toast.LENGTH_SHORT).show();
            return;
        }
        if(str_trans_id.equalsIgnoreCase(""))
        {
            Toast.makeText(this,"Please Enter The Transaction Id",Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.show();
        Call<JsonElement> aboutCall= apiRequests.donate(sharedPreferences.getString("id","0"),str_amount,str_trans_id,str_remark);
        aboutCall.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response)
            {
                try
                {
                    JSONObject jsonObject=new JSONObject(response.body().toString());

                    if(jsonObject.getString("res_status").equalsIgnoreCase("200"))
                    {
                        progressDialog.cancel();
                        Toast.makeText(context,"Success!",Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(UpdateDonarActivity.this,DashboardActivity.class);
                        startActivity(i);
                    }
                    else
                    {
                        progressDialog.cancel();
                        Toast.makeText(context,"Oops Went Something wrong...",Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    progressDialog.cancel();
                    Toast.makeText(context,"Oops Went Something wrong...",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.cancel();
                Toast.makeText(context,"Oops Went Something wrong...",Toast.LENGTH_SHORT).show();
            }
        });





    }


}
