package com.nextgen.spsl;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.nextgen.spsl.Network.APIClient;
import com.nextgen.spsl.Network.APIRequests;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity {

    Button register;

    EditText name,email,mobile,address,password,remark;
    String str_name,str_email,str_mobile,str_address,str_password,str_remark;

    private  static APIRequests apiRequests;
    Context context;
    ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_registration);
        register=findViewById(R.id.register);
        context=this;
        progressDialog=new ProgressDialog(context);
        progressDialog.setMessage("Please Wait");
        apiRequests = APIClient.getInstance().create(APIRequests.class);
        name=findViewById(R.id.name);
        email=findViewById(R.id.email);
        password=findViewById(R.id.password);
        mobile=findViewById(R.id.mobile);
        remark=findViewById(R.id.remark);
        address=findViewById(R.id.address);



        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              checkValidation();

            }
        });
    }

    public void checkValidation()
    {
        str_name=name.getText().toString();
        str_email=email.getText().toString();
        str_password=password.getText().toString();
        str_address=address.getText().toString();
        str_remark=remark.getText().toString();
        str_mobile=mobile.getText().toString();


        if(str_name.equalsIgnoreCase(""))
        {
            Toast.makeText(this,"Please Enter The Name",Toast.LENGTH_SHORT).show();
            return;
        }
        if(str_mobile.equalsIgnoreCase(""))
        {
            Toast.makeText(this,"Please Enter The Mobile",Toast.LENGTH_SHORT).show();
            return;
        }
        if(str_email.equalsIgnoreCase(""))
        {
            Toast.makeText(this,"Please Enter The Email Address",Toast.LENGTH_SHORT).show();
            return;
        }
        if(str_address.equalsIgnoreCase(""))
        {
            Toast.makeText(this,"Please Enter The Address",Toast.LENGTH_SHORT).show();
            return;
        }
        if(str_password.equalsIgnoreCase(""))
        {
            Toast.makeText(this,"Please Enter The Password",Toast.LENGTH_SHORT).show();
            return;
        }
        if(str_remark.equalsIgnoreCase(""))
        {
            Toast.makeText(this,"Please Enter The Remark",Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.show();
        Call<JsonElement> aboutCall= apiRequests.registration(str_name,str_email,str_mobile,str_password,str_address,str_remark);
        aboutCall.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response)
            {
                try
                {
                    JSONObject jsonObject=new JSONObject(response.body().toString());

                    if(jsonObject.getString("res_status").equalsIgnoreCase("200"))
                    {
                        progressDialog.cancel();
                        Toast.makeText(context,"Success!",Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(RegistrationActivity.this,DashboardActivity.class);
                        startActivity(i);

                    }
                    else
                    {
                        progressDialog.cancel();
                        Toast.makeText(context,"Oops Went Something wrong...",Toast.LENGTH_SHORT).show();

                    }


                }
                catch (Exception e)
                {
                    progressDialog.cancel();
                    Toast.makeText(context,"Oops Went Something wrong...",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.cancel();
                Toast.makeText(context,"Oops Went Something wrong...",Toast.LENGTH_SHORT).show();
            }
        });
    }
}



