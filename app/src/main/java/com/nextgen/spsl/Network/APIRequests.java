package com.nextgen.spsl.Network;
import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface APIRequests {

    @GET
    Call<JsonElement> getData(@Url String url);

    @FormUrlEncoded
    @POST("my-master.php?api_key=NEXTGENSOL@123&action=membersignup")
    Call<JsonElement>registration(@Field("name") String name, @Field("email") String email,@Field("mobile") String mobile,@Field("password") String password,@Field("address") String address,@Field("remark") String remark);

    @FormUrlEncoded
    @POST("my-master.php?api_key=NEXTGENSOL@123&action=authMember")
    Call<JsonElement>login(@Field("user_id") String user_id, @Field("password") String password);


    @FormUrlEncoded
    @POST("my-master.php?api_key=NEXTGENSOL@123&action=donate")
    Call<JsonElement>donate(@Field("user_id") String user_id, @Field("amount") String amount, @Field("transaction_id") String transaction_id, @Field("remark") String remark);

    @FormUrlEncoded
    @POST("my-master.php?api_key=NEXTGENSOL@123&action=receive")
    Call<JsonElement>receive(@Field("user_id") String user_id, @Field("name") String name, @Field("query") String query, @Field("mobile") String mobile,@Field("address") String address,@Field("acno") String acno,@Field("ifsc") String ifsc,@Field("branch") String branch);



}
