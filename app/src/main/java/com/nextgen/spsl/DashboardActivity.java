package com.nextgen.spsl;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.nextgen.spsl.Network.APIClient;
import com.nextgen.spsl.Network.APIRequests;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends AppCompatActivity {


    ViewPager viewPager;
    int images[] = {R.drawable.image1, R.drawable.image2, R.drawable.image3};
    EndLessAdapter myCustomPagerAdapter;
    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 4000;


    // private  static APIRequests apiRequests;
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences.Editor editor;

    CardView cv_donar,cv_help,cv_donar_list,cv_receiver_list;

    LinearLayout li_more,li_aboutus,logout,li_report;
    RecyclerView recycleview;

    ArrayList<VideoMoadl> videoModalArrayList;
    private  static APIRequests apiRequests;
    Context context;

    TextView name,detail,newk,slogan,bank_detail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_dashboard);
        apiRequests = APIClient.getInstance().create(APIRequests.class);
        context=this;
        name=findViewById(R.id.name);
        newk=findViewById(R.id.newk);
        detail=findViewById(R.id.detail);
        bank_detail=findViewById(R.id.bank_detail);
        slogan=findViewById(R.id.slogan);

        cv_donar=findViewById(R.id.cv_donar);
        cv_help=findViewById(R.id.cv_help);
        li_more=findViewById(R.id.li_more);
        recycleview=findViewById(R.id.recycleview);
        li_aboutus=findViewById(R.id.li_aboutus);
        li_report=findViewById(R.id.li_report);
        logout=findViewById(R.id.logout);
        cv_donar_list=findViewById(R.id.cv_donar_list);
        cv_receiver_list=findViewById(R.id.cv_receiver_list);
        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        viewPager = findViewById(R.id.viewPager);
        myCustomPagerAdapter = new EndLessAdapter(this, images);
        viewPager.setAdapter(myCustomPagerAdapter);
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        viewPager.setOffscreenPageLimit(1);
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                viewPager.setCurrentItem(currentPage++, true);
            }
        };
        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
        try {
            Field mScroller;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            FixedSpeedScroller scroller = new FixedSpeedScroller(viewPager.getContext());
            mScroller.set(viewPager, scroller);
        } catch (NoSuchFieldException e) {
        } catch (IllegalArgumentException e) {
        } catch (IllegalAccessException e) {
        }
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                currentPage=position;
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });



        cv_donar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(DashboardActivity.this,UpdateDonarActivity.class);
                startActivity(i);
            }
        });

        cv_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(DashboardActivity.this,UpdateReceiverActivity.class);
                startActivity(i);
            }
        });

        cv_donar_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(DashboardActivity.this,DonarListActivity.class);
                startActivity(i);
            }
        });

        cv_receiver_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(DashboardActivity.this,ReceiverListActivity.class);
                startActivity(i);
            }
        });

        li_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(DashboardActivity.this,ContactusActivity.class);
                startActivity(i);
            }
        });

        li_aboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(DashboardActivity.this,AboutUsActivity.class);
                startActivity(i);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.clear();
                editor.commit();

                Intent i=new Intent(DashboardActivity.this,LoginActivity.class);
                startActivity(i);
            }
        });

        li_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String full_str="";
                full_str=name.getText().toString()+detail.getText().toString()+newk.getText().toString()+slogan.getText().toString()+bank_detail.getText().toString();
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,full_str);
                sendIntent.setType("text/plain");
                Intent.createChooser(sendIntent,"Share via");
                startActivity(sendIntent);
            }
        });
        getVideo();
    }



    public void getVideo()
    {

        videoModalArrayList=new ArrayList<>();
        Call<JsonElement> aboutCall= apiRequests.getData("my-master.php?api_key=NEXTGENSOL@123&action=getVideo");
        aboutCall.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response)
            {
                try
                {
                    String res=response.body().toString();
                    JSONObject jsonObject=new JSONObject(res);
                    if(jsonObject!=null && jsonObject.getString("res_status").equalsIgnoreCase("200"))
                    {
                        JSONArray jsonArray=jsonObject.getJSONArray("res_data");
                        for(int i=0;i<jsonArray.length();i++)
                        {
                            VideoMoadl customerModal=new VideoMoadl();
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);

                            String id= jsonObject1.getString("id");
                            customerModal.setId(id);



                            String embed_url= jsonObject1.getString("url");
                            URI uri = new URI(embed_url);
                            String npath = uri.getPath();
                            String idStr = npath.substring(npath.lastIndexOf('/') + 1);
                            customerModal.setUrl(idStr);

                            videoModalArrayList.add(customerModal);
                        }

                        VideoListAdapter videoListAdapter = new VideoListAdapter(context,videoModalArrayList);
                        recycleview.setAdapter(videoListAdapter);
                        recycleview.setHasFixedSize(true);
                        recycleview.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

                    }


                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
            }
        });
    }
}
